package com.can.recipe.app.service;

import com.can.recipe.app.model.Recipe;

import java.util.Set;

public interface RecipeService {

    Set<Recipe> getRecipes();
}
