package com.can.recipe.app.model;


public enum Difficulty {
    EASY,MODERATE,HARD
}
